/* SPDX-License-Identifier: Zlib */

/**
 * Stage: Configure root
 *
 * Configure the root prior to populating it
 *
 * Authors: © 2020-2022 Serpent OS Developers
 * License: ZLib
 */

module boulder.stages.configure_root;

public import boulder.stages : Stage, StageReturn, StageContext;

import mason.build.util : executeCommand, ExecutionError;
import std.algorithm : endsWith, filter, sort;
import std.array : array, join;
import std.experimental.logger;
import std.file : copy, dirEntries, exists, mkdirRecurse, SpanMode, write;
import std.path : dirName;
import std.sumtype : match;

/**
 * Go ahead and configure the tree
 *
 * TODO: Moss should do a lot more heavy lifting here!
 *       Statelessness.
 *       Ensure the moss index is up to date.
 */
public static immutable(Stage) stageConfigureRoot = Stage("configure-root", (StageContext context) {
    /* Root configuration requires confinement */
    if (!context.confinement)
    {
        return StageReturn.Skipped;
    }
    auto repoFile = join([
        context.job.hostPaths.rootfs, "etc/moss/repos.conf.d/99_repo.conf"
    ], "/");
    auto repoDir = repoFile.dirName;
    repoDir.mkdirRecurse();

    auto hostRepoDir = "/etc/moss/repos.conf.d/";
    string chosenHostRepoConfFile = null;
    if (hostRepoDir.exists)
    {
        /* Find any valid config files */
        auto hostMossRepoConfFiles = dirEntries(hostRepoDir, SpanMode.depth).filter!(f => f.name.endsWith(".conf")).array;
        /* Sort reverse alphabetically */
        auto sortedRepoConfFiles = sort!"b > a"(hostMossRepoConfFiles);
        /* Finally choose our config file from the last element */
        chosenHostRepoConfFile = sortedRepoConfFiles[$-1];

        /* FIXME: Needs some validation our config file is correct */

        /* Logging in-case of screwy algorithm */
        tracef("configure-root: Found config files %s", hostMossRepoConfFiles);
        tracef("configure-root: Sorted config files %s", sortedRepoConfFiles);
    }
    /* Use the host's moss repo config file if found */
    if (chosenHostRepoConfFile.length != 0)
    {
        infof("configure-root: Using moss repo config: %s", chosenHostRepoConfFile);
        copy(chosenHostRepoConfFile, repoFile);
    /* FIXME: Don't hardcode the default config file */
    } else {
        infof("configure-root: Using default moss repo config");
        write(repoFile, `
- protosnek:
    description: "Automatically configured remote repository"
    uri: "https://dev.serpentos.com/protosnek/x86_64/stone.index"
`
        );
    }

    string[string] env;
    env["PATH"] = "/usr/bin";
    auto result = executeCommand(context.mossBinary, [
            "ur", "-D", context.job.hostPaths.rootfs
        ], env);
    return result.match!((i) => i == 0 ? StageReturn.Success
        : StageReturn.Failure, (ExecutionError e) => StageReturn.Failure);
});
